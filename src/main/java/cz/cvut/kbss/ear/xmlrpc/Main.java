package cz.cvut.kbss.ear.xmlrpc;

import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;

public class Main {

    public static void main(String[] args) throws Exception {
        // start a web server at port 8080
        WebServer webServer = new WebServer(8080);

        // the xml rpc server lies on top of the web server
        XmlRpcServer xmlRpcServer = webServer.getXmlRpcServer();

        // this object is used to configure the xml rpc server
        PropertyHandlerMapping phm = new PropertyHandlerMapping();

        // add a handler to the server
        phm.addHandler("EarServer", EarServer.class);

        // add the configuration to the xml rpc server
        xmlRpcServer.setHandlerMapping(phm);

        // some boilerplate stuff
        XmlRpcServerConfigImpl serverConfig = (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
        serverConfig.setEnabledForExtensions(true);
        serverConfig.setContentLengthOptional(false);

        // start the web server
        webServer.start();

        System.out.println("Started web server...");
    }
}
