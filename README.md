# XML-RPC Server

A simplistic example of a XML-RPC server.

Run it using `mvn exec:java`. It starts at [localhost:8080](localhost:8080).

Send a **POST** with the following body:

```xml
<?xml version="1.0"?>
<methodCall>
   <methodName>EarServer.hello</methodName>
   <params>
      <param>
         <value><string>Master Chief</string></value>
      </param>
   </params>
</methodCall>
```


## License

LGPLv3
